Implementation of differential drive robot Control using Unity Game engine with C#

To run the simmulation just open build folder and find .exe file.

This is a simulator of differential drive of mobile robot.
You can give different angular rates(w_left, w_right) , radius and wheel distances to the robot as inputs.

![diff5](/uploads/6da9201e1384542e95610f90509fc5e9/diff5.PNG)

![diff4](/uploads/3273775ef4fd7876f02191054b6ea37b/diff4.PNG)

![diff6](/uploads/c42b16ba55ccb473e93830893c13285a/diff6.PNG)