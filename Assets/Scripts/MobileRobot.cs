﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class MobileRobot : MonoBehaviour
{
    #region FIELDS

    [SerializeField]
    private Camera _mainCamera;

    [SerializeField]
    private InputField _radius;

    [SerializeField]
    public InputField _wheelDist;

    [SerializeField]
    public Text _posText;

    [SerializeField]
    public Text _trajMode;

    [SerializeField]
    public Text _wrText;

    [SerializeField]
    public Text _wlText;

    [SerializeField]
    public Text _dtText;

    [SerializeField]
    public Text _timeElapsedText;

    [SerializeField]
    public InputField _rightW;

    [SerializeField]
    public InputField _leftW;

    [SerializeField]
    public Button _runBtn;

    [SerializeField]
    private GameObject [] _wheels;

    [SerializeField]
    private GameObject _targetPoint;

    [SerializeField]
    private LineRenderer _lineRenderer;

    private int _rendererIndex;
    private Vector3 _lastPointRenderered;
    private float _r;
    private float _x, _y;
    private float _v;
    private float _w;
    private float _theta = Mathf.PI / 2;     
    private bool _camView = false;
    private float _timeOffset = 0;
    private bool _stop = true;
    private float _lastTime;
    private float _vr = 0;
    private float _vl = 0;
    private float _cumulativeErr = 0;
    private float _oldErr = 0;
    private float kP = 2f;
    private float kI = 0.04f;
    private float kD = 0.07f;
    private bool _pointClick = false;
    #endregion

    #region MONO BEHAVIOURS
    private void Awake()
    {
        _runBtn.onClick.AddListener(OnClickRun);
    }

    private void Update()
    {
        RenderLine();
        if (Input.GetMouseButtonUp(0) && !_pointClick)
        {
            Vector3 mouse = Input.mousePosition;
            Ray castPoint = Camera.main.ScreenPointToRay(mouse);
            RaycastHit hit;
            if (Physics.Raycast(castPoint, out hit, Mathf.Infinity) && (hit.point.x <= 39 || hit.point.z <= 8))
            {
                _targetPoint.transform.position = hit.point;
                _pointClick = true;
            }
        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            _camView = !_camView; //toggle cammera
            ToggleCameraView();
        }

    }

    private void FixedUpdate()
    {
        if (CalculateDistance(transform.position, _targetPoint.transform.position) && _stop == false)
        {
            DifferentialDrive();
            UniToDiff();
            Move(); //Do the moving;
            Turn(); //Do the turning;
            _timeElapsedText.text = "Time Elapsed: " + (Time.time - _timeOffset);
        }
        else
            _pointClick = false;
        _dtText.text = "dt = " + Time.deltaTime.ToString();

    }
    #endregion

    #region PRIVATE METHODS

    private void InitialConfig()
    {
        //put everything to the start position;
        Quaternion rotate = Quaternion.Euler(0f, 0f, 0f);
        transform.rotation = rotate;
        Vector3 movement = new Vector3(0f, 0f, -20f);
        transform.position = movement;
        _x = transform.position.x;
        _y = transform.position.z;
        _theta = Mathf.PI / 2;
        _timeOffset = Time.time;
    }

    private void OnClickRun()
    {
        InitialConfig();
        _r = float.Parse(_radius.text);
        _vl = float.Parse(_leftW.text);
        _vr = float.Parse(_rightW.text);
        ChangeRobotScale();
        DiffToUni();
        _wrText.text = " Wr = "+ _rightW.text+"(rad/s)";
        _wlText.text = " Wl = "+_leftW.text + "(rad/s)";
        _stop = false;
    }

    private void ChangeRobotScale()
    {
        _wheels[0].transform.localScale = new Vector3(_r * 2, _wheels[0].transform.localScale.y, _r * 2);
        _wheels[1].transform.localScale = new Vector3(_r * 2, _wheels[1].transform.localScale.y, _r * 2);
        transform.localScale = new Vector3(float.Parse(_wheelDist.text), transform.localScale.y, transform.localScale.z);
    }

    private void ToggleCameraView()
    {
        if(_camView ==false)
        {
            Vector3 camposition = new Vector3(-6.5f, 55.6f, -24.13f);
            Quaternion camRot = Quaternion.Euler(70f, 0f, 0f);
            _mainCamera.transform.position = camposition;
            _mainCamera.transform.rotation = camRot;
        }
        else
        {
            Vector3 camposition = new Vector3(32.6f, 55.6f, -35.3f);
            Quaternion camRot = Quaternion.Euler(46.51f, -62.58f, 5.3f);
            _mainCamera.transform.position = camposition;
            _mainCamera.transform.rotation = camRot;
        }
    }

    private void DiffToUni()
    {       
        _v = _r * (_vr + _vl) / 2f;
        _w = _r * (_vr - _vl) / float.Parse(_wheelDist.text);
    }

    private void DifferentialDrive()
    {
        PIDControl();
        _x = _x + _v * Time.deltaTime* Mathf.Cos(_theta);
        _y = _y + _v * Time.deltaTime * Mathf.Sin(_theta);
        _theta = _theta + _w * Time.deltaTime;
    }

    private void UniToDiff()
    {
        _vr = (2 * _v + _w * float.Parse(_wheelDist.text)) / (2 * _r);
        _vl = (2 * _v - _w * float.Parse(_wheelDist.text)) / (2 * _r);

        _wrText.text = " Wr = " + _vr + "(rad/s)";
        _wlText.text = " Wl = " + _vl + "(rad/s)";
    }

    private void PIDControl()
    {
        float distanceToGoalY = _targetPoint.transform.position.z - transform.position.z;
        float distanceToGoalX = _targetPoint.transform.position.x - transform.position.x;

        // Angle from robot to goal
        float gtheta = Mathf.Atan2(distanceToGoalY , distanceToGoalX);

        // Error between the goal angle and robot angle
        float alpha = gtheta - _theta;

        //alpha = g_theta - math.radians(90)
        float e = Mathf.Atan2(Mathf.Sin(alpha), Mathf.Cos(alpha));

        float e_P = e;
        float e_I = _cumulativeErr + e;
        float e_D = e - _oldErr;

        // This PID controller only calculates the angular velocity with constant speed of v
        // The value of v can be specified by giving in parameter or using the pre-defined value defined above.
        _w = kP * e_P + kI * e_I + kD * e_D;

        _w = Mathf.Atan2(Mathf.Sin(_w), Mathf.Cos(_w));

        _cumulativeErr = _cumulativeErr + e;
        _oldErr = e;
    }

    
    private void Move()
    {
        Vector3 movement = new Vector3((_x) , 0f, (_y));
        transform.position = movement;
    }

    private void Turn()
    {
        float theta_deg = Mathf.Rad2Deg*_theta ;
        Quaternion rotate = Quaternion.Euler(0f, theta_deg, 0f);
        transform.rotation = rotate;
        float thdeg = Mathf.Rad2Deg * _theta;
        string txt = "[x y theta] = [" + _x.ToString() + "  " + _y.ToString() + " " + thdeg.ToString() + "]";
        _posText.text = txt;
    }

    private void RenderLine()
    {
        if (CalculateDistance(transform.position, _lastPointRenderered))
        {
            Debug.Log("[time x y theta] = [" + _timeElapsedText.text + "  " + _x.ToString() + "  " + _y.ToString() + " " + (_theta * Mathf.Rad2Deg).ToString() + "]");
            _lineRenderer.positionCount++;
            _lineRenderer.SetPosition(_rendererIndex++, transform.position);
            _lastPointRenderered = transform.position;
        }
    }

    private bool CalculateDistance(Vector3 posNow, Vector3 prevPos)
    {
        if (Vector3.Distance(posNow, prevPos) >= 0.4f)
            return true;
        return false;
    }
    #endregion
}